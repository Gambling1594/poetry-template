# poetry-template

Template repository for python CLI project with poetry.

## Configuration

First, edit your package name in `pyproject.toml`.

```toml
[tool.poetry]
name = "poetry-template"  # Change this package name
version = "0.1.0"
description = "Template repository for python project with poetry."
authors = ["demerara151 <demeraraonfire@gmail.com>"]
license = "MIT"
readme = "README.md"
packages = [{include = "src"}]

[tool.poetry.scripts]
command = "src.main:app"  # Rename command's name you want

---snip---
```

## Installation

```powershell
poetry install
```
